#include <sourcemod>
#include <sdktools>

public Plugin myinfo = 
{
	name = "Create Props",
	author = "The Doggy",
	description = "Adds prop functionality.",
	version = "1.0",
	url = "http://coldcommunity.com"
};

public void OnPluginStart()
{
	RegAdminCmd("sm_create", Command_CreateFurniture, ADMFLAG_CUSTOM2, "Spawns the specified model as a physics prop.");
}

public Action Command_CreateFurniture(int Client, int numParams)
{
	if(Client == 0 || Client > MaxClients)
	{
		PrintToServer("[SM] This command can only be executed in-game.");
		return Plugin_Handled;
	}
	
	if(numParams < 1)
	{
		PrintToChat(Client, "[SM] Invalid Syntax: db_create <model>");
		return Plugin_Handled;
	}
	
	char sModel[64]; GetCmdArg(1, sModel, sizeof(sModel));
	ReplaceString(sModel, sizeof(sModel), "\\", "/", true);

	int iEnt = CreateEntityByName("prop_physics_override");
	if (!IsModelPrecached(sModel))
		PrecacheModel(sModel, true);
	SetEntityModel(iEnt, sModel);
	DispatchSpawn(iEnt);
	
	float fOrigin[3], fEyes[3], fPropOrigin[3];
	GetClientAbsOrigin(Client, fOrigin);
	GetClientEyeAngles(Client, fEyes);
	fPropOrigin[0] = fOrigin[0] + (50 * Cosine(DegToRad(fEyes[1])));
	fPropOrigin[1] = fOrigin[1] + (50 * Sine(DegToRad(fEyes[1])));
	fPropOrigin[2] = fOrigin[2] + 100;

	TeleportEntity(iEnt, fPropOrigin, NULL_VECTOR, NULL_VECTOR);
	AcceptEntityInput(iEnt, "EnableMotion");
	
	PrintToChat(Client, "[SM] Spawned prop.");
	return Plugin_Handled;
}