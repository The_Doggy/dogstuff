#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <morecolors>
#include <bluerp/BluRP>
#include <bluerp>
#define REQUIRE_PLUGIN

#pragma semicolon 1
#pragma newdecls required

//Integers
int g_iNPCDamage[MAXPLAYERS + 1] = 0;

//Strings
char g_sRopes[][] = {
	{"move_rope"},
	{"keyframe_rope"}
};

//Plugin Information:
public Plugin myinfo = 
{
	name = "Rp_Kist", 
	author = "The Doggy", 
	description = "Plugin for implementing Uberkist's map events with BluRP.", 
	version = "1.0", 
	url = "coldcommunity.com"
};

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public void OnPluginStart()
{
	if(!LibraryExists("bluerp"))
		return;
}

public void SQL_GenericQuery(Handle hDB, Handle results, char[] sError, any param)
{
	if (results == INVALID_HANDLE)
	{
		PrintToServer("%s Failed to execute query due to MySQL error: %s", CONSOLETAG, sError);
		LogError("%s Failed to execute query due to MySQL error: %s", CMDTAG, sError);
		return;
	}
}

public void OnMapStart()
{
	for (int i = 0; i < sizeof(g_sRopes); i++) //cheers berni
	{
		int entity = -1;
		while ((entity = FindEntityByClassname(entity, g_sRopes[i])) != INVALID_ENT_REFERENCE)
		{
			AcceptEntityInput(entity, "Kill");
		}
	}
}

public void OnEntityCreated(int entity, const char[] classname)
{
	if(StrEqual(classname, "npc_strider", false) || StrEqual(classname, "npc_helicopter", false) || StrEqual(classname, "npc_combinegunship", false))
	{
		SDKHook(entity, SDKHook_OnTakeDamage, OnTakeDamage);
		HookEntityOutput(classname, "OnDeath", Event_NPCDeath);
	}
	else if(StrEqual(classname, "gib", false) || StrEqual(classname, "helicopter_chunk", false) || StrEqual(classname, "prop_ragdoll") || StrEqual(classname, "item_battery"))
	{
		CreateTimer(10.0, dissolveGib, entity);
	}
}

public Action OnTakeDamage(int victim, int &attacker, int &inflictor,  float &damage, int &damagetype)
{
	if(attacker > 0 && attacker <= MaxClients) 
	{
		char steam[32]; 
		GetClientAuthId(attacker, AuthId_Steam2, steam, sizeof(steam));

		char class[32];
		GetEdictClassname(victim, class, sizeof(class));
		if(StrContains(class, "npc_", false) != -1)
		{
			g_iNPCDamage[attacker] += RoundToNearest(damage);
		}
	}
}

public void Event_NPCDeath(const char[] output, int caller, int activator, float delay)
{
	char class[32];
	GetEdictClassname(caller, class, sizeof(class));

	if (activator == 0 || !IsValidClient(activator))
	{
		return;
	}

	if(StrEqual(class, "npc_helicopter", false))
	{
		//Give money and reset damage values:
		int totalHealth = 0;	
		for(int a = 1; a <= MaxClients; a++)
		{
			totalHealth += g_iNPCDamage[a]; //Total damage dealt
		}

		for(int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i))
			{
				if(g_iNPCDamage[i] > 250)
				{
					BClient player = GetPlayerInstance(i);
					if (!player.IsValid)
					{
						//player is not valid
						continue;
					}

					float dpsPercent = FloatDiv(float(g_iNPCDamage[i]), float(totalHealth));
					int playerGain = RoundToCeil(FloatMul(dpsPercent, 15000.0));
					player.Bank += playerGain;
					player.Chat("%s You gain $%i for dealing %i%% damage to the helicopter", CMDTAG, playerGain, RoundToNearest(dpsPercent * 100));
				}
				else continue;
			}

			g_iNPCDamage[i] = 0;
		}
		UnhookEntityOutput("npc_helicopter", "OnDeath", Event_NPCDeath);
	}
	else if(StrEqual(class, "npc_strider", false))
	{
		//Give money and reset damage values:
		int totalHealth = 0;	
		for(int a = 1; a <= MaxClients; a++)
		{
			totalHealth += g_iNPCDamage[a]; //Total damage dealt
		}

		for(int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i))
			{
				if(g_iNPCDamage[i] > 250)
				{
					BClient player = GetPlayerInstance(i);
					if (!player.IsValid)
					{
						//player is not valid
						continue;
					}

					float dpsPercent = FloatDiv(float(g_iNPCDamage[i]), float(totalHealth));
					int playerGain = RoundToCeil(FloatMul(dpsPercent, 10000.0));
					player.Bank += playerGain;
					player.Chat("%s You gain $%i for dealing %i%% damage to the strider", CMDTAG, playerGain, RoundToNearest(dpsPercent * 100));
				}
				else continue;
			}

			g_iNPCDamage[i] = 0;
		}
		UnhookEntityOutput("npc_strider", "OnDeath", Event_NPCDeath);
	}
	else if(StrEqual(class, "npc_combinegunship", false))
	{
		//Give money and reset damage values:
		int totalHealth = 0;	
		for(int a = 1; a <= MaxClients; a++)
		{
			totalHealth += g_iNPCDamage[a]; //Total damage dealt
		}

		for(int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i))
			{
				if(g_iNPCDamage[i] > 250)
				{
					BClient player = GetPlayerInstance(i);
					if (!player.IsValid)
					{
						//player is not valid
						continue;
					}

					float dpsPercent = FloatDiv(float(g_iNPCDamage[i]), float(totalHealth));
					int playerGain = RoundToCeil(FloatMul(dpsPercent, 5000.0));
					player.Bank += playerGain;
					player.Chat("%s You gain $%i for dealing %i%% damage to the gunship", CMDTAG, playerGain, RoundToNearest(dpsPercent * 100));
				}
				else continue;
			}

			g_iNPCDamage[i] = 0;
		}
		UnhookEntityOutput("npc_combinegunship", "OnDeath", Event_NPCDeath);
	}
}

void dissolveEdict(int entity)
{
	int entDissolve = CreateEntityByName("env_entity_dissolver");
	char className[32];
	GetEntityClassname(entity, className, sizeof(className));
	if(StrEqual(className, "gib", false) || StrEqual(className, "helicopter_chunk", false) || StrEqual(className, "prop_ragdoll", false) || StrEqual(className, "item_battery"))
	{
		if(entDissolve > 0)
		{
			char tarName[32];
			Format(tarName, sizeof(tarName), "Ent_%i", entity);

			DispatchKeyValue(entity, "targetname", tarName);
			DispatchKeyValue(entDissolve, "target", tarName);
			DispatchKeyValue(entDissolve, "dissolvetype", "3");
			DispatchKeyValue(entDissolve, "magnitude", "50.0");

			int random = GetRandomInt(1, 4);
			char sound[64];
			Format(sound, sizeof(sound), "ambient/levels/citadel/weapon_disintegrate%i.wav", random);
			EmitSoundToAll(sound, entity);

			AcceptEntityInput(entDissolve, "Dissolve");
			AcceptEntityInput(entDissolve, "Kill");
		} 
		else AcceptEntityInput(entity, "Kill");
	}
}

public Action dissolveGib(Handle timer, any data) //creative naming conventions
{
	int entity = data;
	dissolveEdict(entity);
	return Plugin_Handled;
}