# -*- coding: utf-8 -*-

import random

questions = ["How do you say 'love' in Maori? ", "How do you say 'river' in Maori? ", "How do you say 'guests' in Maori? ", "How do you say 'mountain' in Maori? ", "How do you say 'sea' in Maori? ", "How do you say 'island' in Maori? ", "How do you say 'large' in Maori? ", "How do you say 'stomach' in Maori? ", "How do you say 'children' in Maori? ", "How do you say 'funeral' in Maori? ", "How do you say 'water' in Maori? ", "How do you say 'elder' in Maori? ", "How do you say 'work' in Maori? ", "How do you say 'prayer' in Maori? ", "How do you say 'food' in Maori? ", "How do you say 'tribe' in Maori? ", "How do you say 'small' in Maori? ", "How do you say 'meeting' in Maori? ", "How do you say 'walk' in Maori? ", "How do you say 'New Zealand' in Maori? "]
answers = ["aroha", "awa", "manuhiri", "maunga", "moana", "motu", "nui", "kopu", "tamariki", "tanumanga", "wai", "kaumatua", "mahi", "inoi", "kai", "iwi", "iti", "hui", "haere", "aotearoa"] #Sourced from: http://www.maorilanguage.net/maori-words-phrases/50-maori-words-every-new-zealander-know/

#Counter for while loop
count = 0
#Score of player
score = 0
#List to check for duplicate questions
dupe = []

while count < 5:
	rand = random.randint(0, len(questions) - 1)
	
	#Duplication checking
	if questions[rand] not in dupe:
		dupe.append(questions[rand])

		#Doing stuff with answer
		ans = input(questions[rand])
		lowAns = ans.lower()

		#lol
		if lowAns == "mario":
			print("""
	▒▒▒▒▒▒▒▒▒▄▄▄▄▒▒▒▒▒▒▒
	▒▒▒▒▒▒▄▀▀▓▓▓▀█▒▒▒▒▒▒
	▒▒▒▒▄▀▓▓▄██████▄▒▒▒▒
	▒▒▒▄█▄█▀░░▄░▄░█▀▒▒▒▒
	▒▒▄▀░██▄░░▀░▀░▀▄▒▒▒▒
	▒▒▀▄░░▀░▄█▄▄░░▄█▄▒▒▒
	▒▒▒▒▀█▄▄░░▀▀▀█▀▒▒▒▒▒
	▒▒▒▄▀▓▓▓▀██▀▀█▄▀▀▄▒▒
	▒▒█▓▓▄▀▀▀▄█▄▓▓▀█░█▒▒
	▒▒▀▄█░░░░░█▀▀▄▄▀█▒▒▒
	▒▒▒▄▀▀▄▄▄██▄▄█▀▓▓█▒▒
	▒▒█▀▓█████████▓▓▓█▒▒
	▒▒█▓▓██▀▀▀▒▒▒▀▄▄█▀▒▒
	▒▒▒▀▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒""")
			print("\nIT'S-A-ME, MARIO!\n")
			score = 1337
			break

		#Actual answer checking
		elif lowAns == answers[rand]:
			print("Correct!\n")
			count += 1
			score += 1
		else:
			print("Incorrect\n")
			count += 1

#Game end
if score > 0 and score <= 5:
	print("You got %d out of 5 correct!" %(score))
elif score == 0:
	print("You didn't get any right :(")

input("\nPress the enter key to exit.")
