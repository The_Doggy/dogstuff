package com.stevej42.bankaccount;

import java.sql.SQLException;

public class BankAccount {

    private double Balance;
    private int Id;
    private double Interest;
    private double loanAmt;
    private String Name;
    private String Type;

    public BankAccount(int id) {
        this.Balance = 0.0;
        this.Id = id;
        this.Interest = 5.0;
        this.loanAmt = 0.0;
        this.Name = "";
        this.Type = "";
    }

    public BankAccount(double balance, int id, double interest) {
        this.Balance = balance;
        this.Id = id;
        this.Interest = interest;
        this.loanAmt = 0.0;
        this.Name = "";
        this.Type = "";
    }

    public BankAccount(double balance, int id, double interest, String name, String type) {
        this.Balance = balance;
        this.Id = id;
        this.Interest = interest;
        this.loanAmt = 0.0;
        this.Name = name;
        this.Type = type;
    }

    public BankAccount(double balance, int id, double interest, double loanamt, String name, String type) {
        this.Balance = balance;
        this.Id = id;
        this.Interest = interest;
        this.loanAmt = loanamt;
        this.Name = name;
        this.Type = type;
    }

    public int getId() {
        return Id;
    }

    public double getInterest() {
        return Interest;
    }

    public void setInterest(double interest) {
        Interest = interest;
        this.Save();
    }

    public double getBalance() {
        return Balance;
    }

    public double getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(double loanAmt) {
        this.loanAmt = loanAmt;
        this.Save();
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
        this.Save();
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
        this.Save();
    }

    public void Deposit(Double amount) {
        this.Balance += amount;
        this.Save();
    }

    public void Withdraw(Double amount) {
        if (this.Balance - amount < -1000)
            this.Balance = -1000;
        else
            this.Balance -= amount;
        this.Save();
    }

    public void payLoan(Double amount) {
        if (amount > this.loanAmt) {
            double remainder = amount - this.loanAmt;
            this.Deposit(remainder);
            this.loanAmt = 0;
        } else
            this.loanAmt -= amount;
        this.Save();
    }

    public void Save() {
        try {
            SQLManager.saveBankAccount(this);
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            SQLManager.connectToMySQL();
        }
    }
}
