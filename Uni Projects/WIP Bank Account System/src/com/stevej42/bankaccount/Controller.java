package com.stevej42.bankaccount;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.sql.SQLException;

public class Controller {

    //Sign in button stuff
    @FXML
    private Text errorText;
    @FXML
    private TextField user;
    @FXML
    private PasswordField pass;

    //Registering stuff
    @FXML
    private Text regError;
    @FXML
    private Text infoText;
    @FXML
    private TextField regUser;
    @FXML
    private PasswordField regPass;

    //Main stuff
    @FXML
    private Text mainInfo;

    //Deposit stuff
    @FXML
    private Text depError;
    @FXML
    private Text depInfo;
    @FXML
    private TextField depAmount;

    //Withdraw stuff
    @FXML
    private Text withdrawError;
    @FXML
    private Text withdrawInfo;
    @FXML
    private TextField withdrawAmount;

    //Get loan stuff
    @FXML
    private Text loanError;
    @FXML
    private Text loanInfo;
    @FXML
    private TextField loanAmount;

    //Pay loan stuff
    @FXML
    private Text pLoanError;
    @FXML
    private Text pLoanInfo;
    @FXML
    private TextField pLoanAmount;

    @FXML
    protected void handleSignInButtonAction(ActionEvent event) {
        try {
            SQLManager.ErrorType type = SQLManager.checkLogin(user.getText(), pass.getText());
            if (type == SQLManager.ErrorType.e_None) {
                try {
                    Main.changeScene("main.fxml");
                } catch (Exception e) {
                    System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
                }
            } else if (type == SQLManager.ErrorType.e_WrongPassword)
                errorText.setText("Invalid Username/Password.");
            else if (type == SQLManager.ErrorType.e_SQLError)
                errorText.setText("Error occurred while trying to sign in, please try again.");
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            SQLManager.connectToMySQL();
        }
    }

    @FXML
    protected void handleRegisterButtonAction(ActionEvent event) {
        try {
            Main.changeScene("register.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handleRegBackButtonAction(ActionEvent event) {
        try {
            Main.changeScene("Login.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handleRegSubmitButtonAction(ActionEvent event) {
        if (regUser.getText().trim().isEmpty() || regPass.getText().trim().isEmpty()) {
            infoText.setText("");
            regError.setText("Username/Password fields cannot be empty.");
            return;
        }
        try {
            SQLManager.ErrorType type = SQLManager.createUser(regUser.getText(), regPass.getText());
            if (type == SQLManager.ErrorType.e_None) {
                regError.setText("");
                infoText.setText("Successfully Registered. Press the back button to sign in.");
            } else if (type == SQLManager.ErrorType.e_DuplicateUser) {
                infoText.setText("");
                regError.setText("Username already exists.");
                return;
            } else {
                infoText.setText("");
                regError.setText("Unknown error occurred, please try again.");
                return;
            }
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            SQLManager.connectToMySQL();
        }
    }

    @FXML
    protected void handleMainDepositButtonAction(ActionEvent event) {
        try {
            Main.changeScene("deposit.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handleDepositButtonAction(ActionEvent event) {
        if (depAmount.getText().trim().isEmpty()) {
            depInfo.setText("");
            depError.setText("Deposit amount cannot be blank.");
            return;
        }

        double amount = Double.parseDouble(depAmount.getText());
        if (amount <= 0.00) {
            depInfo.setText("");
            depError.setText("Deposit amount must be greater than 0.");
            return;
        }

        BankAccount acc = SQLManager.getBankAccount();
        acc.Deposit(amount);
        depError.setText("");
        depInfo.setText("Deposited $" + depAmount.getText() + " into your account.");
    }

    @FXML
    protected void handleMainWithdrawButtonAction(ActionEvent event) {
        try {
            Main.changeScene("withdraw.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handleWithdrawButtonAction(ActionEvent event) {
        if (withdrawAmount.getText().trim().isEmpty()) {
            withdrawInfo.setText("");
            withdrawError.setText("Withdrawal amount cannot be blank.");
            return;
        }

        double amount = Double.parseDouble(withdrawAmount.getText());
        if (amount <= 0.00) {
            withdrawInfo.setText("");
            withdrawError.setText("Withdrawal amount must be greater than 0.");
            return;
        }

        BankAccount acc = SQLManager.getBankAccount();
        acc.Withdraw(amount);
        withdrawError.setText("");
        withdrawInfo.setText("Withdrew $" + withdrawAmount.getText() + " from your account.");
    }

    @FXML
    protected void handleMainBalanceButtonAction(ActionEvent event) {
        BankAccount acc = SQLManager.getBankAccount();
        mainInfo.setText("Your balance is: $" + acc.getBalance());
    }

    @FXML
    protected void handleMainGetLoanButtonAction(ActionEvent event) {
        try {
            Main.changeScene("getLoan.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handleGetLoanButtonAction(ActionEvent event) {
        BankAccount acc = SQLManager.getBankAccount();
        if (acc.getLoanAmt() > 0) {
            loanInfo.setText("");
            loanError.setText("You already have an outstanding loan.");
            return;
        }

        if (loanAmount.getText().trim().isEmpty()) {
            loanInfo.setText("");
            loanError.setText("Loan amount cannot be blank.");
            return;
        }

        double amount = Double.parseDouble(loanAmount.getText());
        if (amount <= 0.00) {
            loanInfo.setText("");
            loanError.setText("Loan amount must be greater than 0.");
            return;
        }

        acc.setLoanAmt(amount);
        loanError.setText("");
        loanInfo.setText("$" + loanAmount.getText() + " loan added to your account.");
    }

    @FXML
    protected void handleMainPayLoanButtonAction(ActionEvent event) {
        try {
            Main.changeScene("payLoan.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handlePayLoanButtonAction(ActionEvent event) {
        BankAccount acc = SQLManager.getBankAccount();
        if (acc.getLoanAmt() == 0) {
            pLoanInfo.setText("");
            pLoanError.setText("You do not have a loan to pay.");
            return;
        }

        if (pLoanAmount.getText().trim().isEmpty()) {
            pLoanInfo.setText("");
            pLoanError.setText("Loan amount cannot be blank.");
            return;
        }

        double amount = Double.parseDouble(pLoanAmount.getText());
        if (amount <= 0.00) {
            pLoanInfo.setText("");
            pLoanError.setText("Loan amount must be greater than 0.");
            return;
        }

        acc.payLoan(amount);
        pLoanError.setText("");
        pLoanInfo.setText("Paid $" + pLoanAmount.getText() + " off your loan. $" + acc.getLoanAmt() + " remaining.");
    }

    @FXML
    protected void handleMainLoanButtonAction(ActionEvent event) {
        BankAccount acc = SQLManager.getBankAccount();
        if (acc.getLoanAmt() == 0)
            mainInfo.setText("You do not have a loan.");
        else
            mainInfo.setText("Loan Balance: $" + acc.getLoanAmt());
    }

    @FXML
    protected void handleMainBackButtonAction(ActionEvent event) {
        try {
            Main.changeScene("main.fxml");
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.getClass().getCanonicalName());
        }
    }

    @FXML
    protected void handleExitButtonAction(ActionEvent event) {
        Platform.exit();
    }
}
