package com.stevej42.bankaccount;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage stage;

    public static void changeScene(String fxml) throws Exception {
        Parent root = FXMLLoader.load(Main.class.getResource(fxml));
        Scene scene = stage.getScene();
        if (scene == null) {
            scene = new Scene(root, 400, 375);
            stage.setScene(scene);
        } else
            stage.getScene().setRoot(root);
        stage.sizeToScene();
    }

    public static void main(String[] args) {
        SQLManager.init();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.setTitle("Bankcorp Inc.");
        primaryStage.setScene(new Scene(root, 400, 375));
        primaryStage.show();
    }

    @Override
    public void stop() {
        System.out.println("Application closing.");
        BankAccount acc = SQLManager.getBankAccount();
        if (acc != null)
            acc.Save();
    }
}
