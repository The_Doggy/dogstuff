package com.stevej42.bankaccount;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.*;

import static com.stevej42.bankaccount.SQLManager.ErrorType.*;

public class SQLManager {
    private static Connection conn = null;
    private static PreparedStatement stmt = null;
    private static ResultSet rs = null;

    private static BankAccount g_Bank = null;

    public static void init() {
        connectToMySQL();
        createTables();
    }

    static void connectToMySQL() {
        //Database go here xd
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser("");
        dataSource.setPassword("");
        dataSource.setServerName("");
        dataSource.setDatabaseName("");

        try {
            conn = dataSource.getConnection();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    static void createTables() {
        if (conn == null) {
            connectToMySQL();
            return;
        }

        try {
            Statement stmt = conn.createStatement();
            //TODO: change password to another datatype after implementation done? e.g. binary(60)
            stmt.execute("CREATE TABLE IF NOT EXISTS account (id INTEGER NOT NULL AUTO_INCREMENT, balance DECIMAL(15,2) NOT NULL DEFAULT 0, interest FLOAT(5,2) NOT NULL DEFAULT 0, type VARCHAR(16), " +
                    "name VARCHAR(32), loan_amount DECIMAL(15,2) NOT NULL DEFAULT 0, PRIMARY KEY(id));");
            stmt.execute("CREATE TABLE IF NOT EXISTS user (username VARCHAR(64) NOT NULL, password VARCHAR(60) NOT NULL, account_id INTEGER NOT NULL AUTO_INCREMENT, PRIMARY KEY(username), " +
                    "FOREIGN KEY(account_id) REFERENCES account(id));");
        } catch (SQLException e) {
            printSQLException(e);
        } finally {
            //release resources
            close();
        }
    }

    static ErrorType checkLogin(String username, String password) throws SQLException {
        if (conn == null) {
            connectToMySQL();
            throw new SQLException("Cannot connect to MySQL Database.");
        }

        try {
            stmt = conn.prepareStatement("SELECT password FROM user WHERE ? = username;");
            stmt.setString(1, username);

            //get resultset from query
            rs = stmt.executeQuery();

            //check for results
            if (rs.next()) {
                String dbPass = rs.getString(1);
                if (BCrypt.checkpw(password, dbPass)) {
                    loadBankAccount(username);
                } else
                    return e_WrongPassword;
            } else
                return e_WrongPassword;
        } catch (SQLException e) {
            printSQLException(e);
            return e_SQLError;
        } finally {
            //release resources
            close();
        }
        return e_None;
    }

    static void loadBankAccount(String user) throws SQLException {
        if (conn == null) {
            connectToMySQL();
            throw new SQLException("Cannot connect to MySQL Database.");
        }

        try {
            stmt = conn.prepareStatement("SELECT balance, id, interest, loan_amount, type, name FROM account WHERE id = (SELECT account_id FROM user WHERE username = ?);");
            stmt.setString(1, user);
            rs = stmt.executeQuery();

            if (rs.next()) {
                g_Bank = new BankAccount(rs.getDouble(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4), rs.getString(5), rs.getString(6));
            }
        } catch (SQLException e) {
            printSQLException(e);
        } finally {
            close();
        }
    }

    public static BankAccount getBankAccount() {
        return g_Bank;
    }

    static ErrorType createUser(String user, String pass) throws SQLException {
        if (conn == null) {
            connectToMySQL();
            throw new SQLException("Cannot connect to MySQL Database.");
        }

        try {
            PreparedStatement selectUser = conn.prepareStatement("SELECT username FROM user WHERE username = ?");
            selectUser.setString(1, user);
            rs = selectUser.executeQuery();

            //If any rows were returned from the query then the username already exists
            if (rs.isBeforeFirst())
                return e_DuplicateUser;

            //TODO: Change interest based on account type
            PreparedStatement accInsert = conn.prepareStatement("INSERT INTO account (interest) VALUES (15.0);");
            accInsert.executeUpdate();
            accInsert.close();

            stmt = conn.prepareStatement("INSERT INTO user (username, password) VALUES (?, ?);");
            stmt.setString(1, user);
            String hashedPw = BCrypt.hashpw(pass, BCrypt.gensalt());
            stmt.setString(2, hashedPw);
            stmt.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
            return e_SQLError;
        } finally {
            close();
        }
        return e_None;
    }

    static void saveBankAccount(BankAccount account) throws SQLException {
        if (conn == null) {
            connectToMySQL();
            throw new SQLException("Cannot connect to MySQL Database.");
        }

        try {
            stmt = conn.prepareStatement("INSERT INTO account (id, balance, interest, type, name, loan_amount) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE balance=?, interest=?, type=?, name=?, loan_amount=?;");
            stmt.setInt(1, account.getId());
            stmt.setDouble(2, account.getBalance());
            stmt.setDouble(3, account.getInterest());
            stmt.setString(4, account.getType());
            stmt.setString(5, account.getName());
            stmt.setDouble(6, account.getLoanAmt());
            stmt.setDouble(7, account.getBalance());
            stmt.setDouble(8, account.getInterest());
            stmt.setString(9, account.getType());
            stmt.setString(10, account.getName());
            stmt.setDouble(11, account.getLoanAmt());
            stmt.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        } finally {
            close();
        }
    }

    static void close() {
        //release resources in reverse-order they are created if no longer needed
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            } //ignore

            rs = null;
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
            } //ignore

            stmt = null;
        }
    }

    private static void printSQLException(SQLException e) {
        System.out.println("SQLException: " + e.getMessage());
        System.out.println("SQLState: " + e.getSQLState());
        System.out.println("VendorError: " + e.getErrorCode());
    }

    public enum ErrorType {
        e_None,
        e_DuplicateUser,
        e_WrongPassword,
        e_SQLError
    }
}
